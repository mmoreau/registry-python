# Registry

A library to simplify the search in the Windows registry while keeping the functions of the "Winreg" module

## Dependencies

* Python 3 (https://www.python.org/downloads/)

## Script
### **Windows**
```python
    from Registry import Registry
```

## Todo
- [ ] Remove
- [ ] Update
- [ ] Filter

## Contact

Feel free to contact me if you have any questions, criticisms, suggestions, bugs encountered 

* **Twitter :** https://twitter.com/mxmmoreau
* **LinkedIn :** https://www.linkedin.com/in/mxmoreau/